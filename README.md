# Simple Photo Planner

Simple Photo Planner (SPHP) is a simple PHP script to keep track of locations you plan to visit and photograph. It can be useful for planning travels and photo walks.

## Dependencies

- PHP 7.x or higher
- Apache or any other web server (optional)

# Installation and Usage

The [Linux Photography](https://gumroad.com/l/linux-photography) book provides instructions on installing and using Simple Photo Planner . Get your copy at [Google Play Store](https://play.google.com/store/books/details/Dmitri_Popov_Linux_Photography?id=cO70CwAAQBAJ) or [Gumroad](https://gumroad.com/l/linux-photography).

<img src="https://scribblesandsnaps.files.wordpress.com/2016/07/linux-photography-6.jpg" width="200"/>

<?php
include('config.php');
if ($protect) {
	require_once('protect.php');
}
?>

<html lang="en">
<!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->

<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<link rel="shortcut icon" href="favicon.png" />
	<link rel="stylesheet" href="css/milligram.min.css">
	<link rel="stylesheet" href="css/styles.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
	<div id="content">
		<h1><?php echo $title; ?></h1>
		<table id="theTable">
			<?php
			// Start session
			if (session_status() == PHP_SESSION_NONE) {
				session_start();
			}
			// The $d parameter is used to detect a subdirectory
			if (isset($_GET['d'])) {
				$current_dir = $_GET['d'];
			} else {
				$current_dir = $root_dir;
			}
			$_SESSION["dir"] = $current_dir;
			$sub_dirs = array_filter(glob($current_dir . DIRECTORY_SEPARATOR . '*'), 'is_dir');
			// Populate a drop-down list with subdirectories
			if ((count($sub_dirs)) > 0 or (!empty($current_dir))) {
				$higher_dirs = explode("/", $current_dir);
				$higher_dir_cascade = "";
				foreach ($higher_dirs as $higher_dir) {
					if (!empty($higher_dir)) {
						if (!empty($higher_dir_cascade)) {
							$higher_dir_cascade = $higher_dir_cascade . DIRECTORY_SEPARATOR;
						}
						$higher_dir_cascade = $higher_dir_cascade . $higher_dir;
						echo "<a href='"  . basename($_SERVER['PHP_SELF']) . "?d=" . $higher_dir_cascade . "'>" . $higher_dir . "</a> /&nbsp;";
					}
				}

				echo '<select style="width: 15em;" name="" onchange="javascript:location.href = this.value;">';
				echo '<option value="Default">Choose destination</option>';
				foreach ($sub_dirs as $dir) {
					$dir_name = basename($dir);
					$dir_option = str_replace('\'', '&apos;', $current_dir . DIRECTORY_SEPARATOR . $dir_name);
					echo "<option value='?d=" . ltrim($dir_option, '/') . "'>" . $dir_name . "</option>";
				}
				echo "</select>";
				echo "</div>";
			}
			// Create the current directory
			if (!file_exists($current_dir)) {
				mkdir($current_dir, 0755, true);
			}
			// Read CSV file
			$csvfile = $current_dir . DIRECTORY_SEPARATOR . "data.csv";
			if (!is_file($csvfile)) {
				$HEADER = "Location;Map;Note\nTokyo;https://goo.gl/maps/y9xsgM49DGG2;Tokyo, Japan";
				file_put_contents($csvfile, $HEADER);
			}
			$row = 1;
			if (($handle = fopen($csvfile, "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
					$num = count($data);
					if ($row == 1) {
						echo '<thead><tr>';
					} else {
						echo '<tr>';
					}
					$value0 = $data[0];
					$value1 = $data[1];
					$value2 = $data[2];
					if ($row == 1) {
						echo '<th class="sortable" onclick="sortTable(0)">' . $value0 . '</th>';
						echo '<th class="sortable" onclick="sortTable(1)">' . $value2 . '</th>';
					} else {
						echo '
					<td><span style="padding-right: 5px">' . $value0 . '</span> <a target="_blank" href="https://www.google.com/search?q=weather+forecast+' . $value0 . '"> <span style="padding: 3px"><i class="fas fa-sun"></i></span></a> <span style="padding-right: 3px"></span> <a target="_blank" href="' . $value1 . '"><span style="padding: 3px"><i class="fas fa-link"></i></span></a></td>';
						echo '<td>' . $value2 . '</td>';
					}
					if ($row == 1) {
						echo '</tr></thead><tbody>';
					} else {
						echo '</tr>';
					}
					$row++;
				}
				fclose($handle);
			}
			?>
			</tbody>
		</table>
		<form method='POST' action='edit.php'>
			<p style="margin-top: 1.5em;"><button type='submit'>Edit</button></p>
		</form>
		<p><?php echo $footer; ?></p>
	</div>
	<script>
		function sortTable(n) {
			var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
			table = document.getElementById("theTable");
			switching = true;
			dir = "asc";
			while (switching) {
				switching = false;
				rows = table.rows;
				for (i = 1; i < (rows.length - 1); i++) {
					shouldSwitch = false;
					x = rows[i].getElementsByTagName("TD")[n];
					y = rows[i + 1].getElementsByTagName("TD")[n];
					if (dir == "asc") {
						if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
							shouldSwitch = true;
							break;
						}
					} else if (dir == "desc") {
						if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
							shouldSwitch = true;
							break;
						}
					}
				}
				if (shouldSwitch) {
					rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
					switching = true;
					switchcount++;
				} else {
					if (switchcount == 0 && dir == "asc") {
						dir = "desc";
						switching = true;
					}
				}
			}
		}
	</script>
</body>

</html>